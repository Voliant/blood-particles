//package com.legacy.blood_particles;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.regex.Pattern;
//
//import net.minecraft.util.ResourceLocation;
//import net.minecraft.util.ResourceLocationException;
//import net.minecraftforge.common.ForgeConfigSpec;
//import net.minecraftforge.common.ForgeConfigSpec.ConfigValue;
//
//public class BloodParticlesConfig {
//	static final ForgeConfigSpec spec;
//	
//	private static final ConfigValue<Map<String, String>> $customParticles;
//	
//	static {
//		ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
//	
//		builder.comment("Blood particles general config")
//			.push("general");
//		
//		$customParticles = builder
//			.comment("A mapping of `<entity id>` to `<particle id> [<block/item id>]` for the blood particles")
//			.define("custom_particles", defaultCustomParticlesMap(), BloodParticlesConfig::validateCustomParticles);
//		
//		builder.pop();
//		
//		spec = builder.build();
//	}
//	
//	private static Map<String, String> defaultCustomParticlesMap() {
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("minecraft:zombie_pigman", "minecraft:block minecraft:lime_terracotta");
//		map.put("minecraft:phantom", "minecraft:block minecraft:gray_stained_glass");
//		map.put("minecraft:blaze", "minecraft:flame");
//		map.put("minecraft:slime", "minecraft:item_slime");
//		map.put("minecraft:magma_cube", "minecraft:magma_block");
//		map.put("minecraft:enderman", "minecraft:block minecraft:purple_concrete");
//		map.put("minecraft:ender_dragon", "minecraft:block minecraft:purple_concrete");
//		map.put("default", "minecraft:block minecraft:redstone_block");
//		return map;
//	}
//	
//	private static final Pattern VALID_PARTICLE_PATTERN = Pattern.compile("^((minecraft:)?(((falling_)?block|item)\\s+([-_.a-z0-9]+:)?[-_.a-z0-9/]+(\\[\\s*(([-_.a-z0-9]+\\s*=\\s*[-_.a-z0-9]+)(\\s*,\\s*([-_.a-z0-9]+\\s*=\\s*[-_.a-z0-9]+))*)?\\s*\\])?$|dust\\s+([-+]?(\\d+))))|(?!(minecraft:)?(((falling_)?block|item|dust))$)([-_.a-z0-9]+:)?[-_.a-z0-9/]+$");
//	
//	private static boolean validateCustomParticles(Object obj) {
//		if (!(obj instanceof Map)) {
//			return false;
//		}
//		
//		Map<?,?> map = (Map<?,?>) obj;
//		
//		for (Map.Entry<?,?> entry : map.entrySet()) {
//			if (!(entry.getKey() instanceof String && entry.getValue() instanceof String)) {
//				return false;
//			}
//			
//			ResourceLocation entityId;
//			try {
//				entityId = new ResourceLocation((String)entry.getKey());
//			}
//			catch (ResourceLocationException e) {
//				return false;
//			}
//			
//			String particleIdStr = (String) entry.getValue();
//			
//			
//		}
//	}
//	
//	
//	
//}
